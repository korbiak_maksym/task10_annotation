package com.korbiak.view;

import com.korbiak.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {

    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input;
    private static Logger logger = LogManager.getLogger(View.class);


    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - Ex 1");
        menu.put("2", "2 - Ex 2");
        menu.put("3", "3 - Ex 3");
        menu.put("4", "4 - Ex 4");
        menu.put("5", "5 - Ex 5");
        menu.put("6", "6 - Ex 6");
        menu.put("Q", "Q - exit");
    }

    public View() {

        controller = new Controller();
        input = new Scanner(System.in);
        setMenu();
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::getEx1);
        methodsMenu.put("2", this::getEx2);
        methodsMenu.put("3", this::getEx3);
        methodsMenu.put("4", this::getEx4);
        methodsMenu.put("5", this::getEx5);
        methodsMenu.put("6", this::getEx6);
    }

    private void getEx6() {
        logger.trace(controller.getEx6());
    }

    private void getEx5() {
        logger.trace(controller.getEx5());
    }

    private void getEx4() {
        logger.trace(controller.getEx4());
    }

    private void getEx3() {
        logger.trace(controller.getEx3());
    }

    private void getEx2() {
        logger.trace(controller.getEx2());
    }

    private void getEx1() {
        logger.trace(controller.getEx1());
    }


    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.trace("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).getCom();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void outputMenu() {
        logger.trace("\nMENU:");
        for (String str : menu.values()) {
            logger.trace(str);
        }
    }
}

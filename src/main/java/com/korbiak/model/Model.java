package com.korbiak.model;

import com.sun.org.apache.bcel.internal.generic.ANEWARRAY;
import com.sun.org.apache.regexp.internal.RE;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Model {

    public String getEx1() {
        StringBuilder answer = new StringBuilder();
        Person person = new Person("Maksym", "Korbiak", 19, "0980265133");
        Class myPerson = person.getClass();
        Field[] fields = myPerson.getDeclaredFields();
        int i = 0;
        for (Field f : fields) {
            i++;
            if (f.isAnnotationPresent(MyAnnotation.class)) {
                answer.append(i).append("- field:");
                answer.append(f.toString()).append("\n");
            }
        }

        return answer.toString();
    }

    public String getEx2() {
        StringBuilder answer = new StringBuilder();
        Person person = new Person("Maksym", "Korbiak", 19, "0980265133");
        Class myPerson = person.getClass();
        Field[] fields = myPerson.getDeclaredFields();
        int i = 0;
        for (Field f : fields) {
            i++;
            if (f.isAnnotationPresent(MyAnnotation.class)) {
                MyAnnotation info = (MyAnnotation) f.getAnnotation(MyAnnotation.class);
                answer.append("\nExpl: " + info.explanation());
            }
        }

        return answer.toString();
    }

    public String getEx3() {
        String answer = "";
        Person person = new Person("Maksym", "Korbiak", 19, "0980265133");
        Class myPerson = person.getClass();
        try {
            Method method = myPerson.getDeclaredMethod("getName");
            answer += "Name:" + method.invoke(person);
            method = myPerson.getDeclaredMethod("getAge");
            answer += "\nAge:" + method.invoke(person);
            method = myPerson.getDeclaredMethod("setAge", int.class);
            method.invoke(person, 20);
            method = myPerson.getDeclaredMethod("getAge");
            answer += "\nNew age:" + method.invoke(person);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

        return answer;
    }

    public String getEx4() {
        Person person = new Person("Maksym", "Korbiak", 19, "0980265133");
        Class myPerson = person.getClass();
        Field field = null;
        try {
            field = myPerson.getDeclaredField("name");
            field.setAccessible(true);
            field.set(person, "Max");
            return "New name:" + field.get(person);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return "no name";
    }

    public String getEx5() {
        String answer = "";
        Person person = new Person("Maksym", "Korbiak", 19, "0980265133");
        Class myPerson = person.getClass();
        Method method = null;
        try {
            method = myPerson.getDeclaredMethod("myMethod", String[].class);
            answer += "First:" + method.invoke(person, new String[]{"1", "2"}) + "\n";
            method = myPerson.getDeclaredMethod("myMethod", String.class, int[].class);
            answer += "Second:" + method.invoke(person, "second", new int[]{1, 2}) + "\n";
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return answer;
    }

    public String getEx6() {
        Person person = new Person("Maksym", "Korbiak", 19, "0980265133");

        return person.showInfo(person);
    }
}



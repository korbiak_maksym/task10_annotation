package com.korbiak.controller;

import com.korbiak.model.Model;

public class Controller {
    Model model;

    public Controller() {
        model = new Model();
    }

    public String getEx1(){
        return model.getEx1();
    }

    public String getEx2(){
        return model.getEx2();
    }

    public String getEx3(){
        return model.getEx3();
    }

    public String getEx4(){
        return model.getEx4();
    }

    public String getEx5(){
        return model.getEx5();
    }

    public String getEx6(){
        return model.getEx6();
    }
}

package com.korbiak.model;

public class Person {

    @MyAnnotation
    private String name;
    private String surname;
    private int age;
    @MyAnnotation(explanation = "mobile phone")
    private String phone;

    public Person(String name, String surname, int age, String phone) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String myMethod(String a, int... args) {
        return "myMethod(String a, int...args)";
    }

    public String myMethod(String... args) {
        return "myMethod(String...args)";
    }

    public <T> String showInfo(T obj) {
        return obj.getClass().toString() +
                "; value = " + obj.toString();
    }

}
